19.03.2020
Märten - failid ja albumid
Kalev - kasutajad ja autentimine

21.03.2020
Märten - albumite loomine
Luua saab sisseloginud kasutaja
Name - kasutaja saab ise panna
Description (Caption) praegu puudu - kasutaja paneb
Created - DateTime.Now
Owner - currentUser
CoverPictureId - albumi loomisel ei näidata - vaid Edit puhul
State - avalik, mitteavalik
CategoryId - ei peaks olema kohustuslik
ParentAlbumId - vaikimisi see album, milles uut albumit tegema hakati

Albumi muutmine

24.03.2020
Sisseloginud kasutaja näeb ja saab muuta oma albumite lehel vaid enda loodud albumeid
Külastaja näeb kõiki albumeid, kuid ei saa midagi muuta

Piltide lisamine
Kuidas näidata pilte. Pildid on ju albumi sisu
5 maastikupildid
7 Album 2

25.03.2020
Ülal menüüs on enda piltide link, kus saab muuta ja kustutada - MyIndex. Albumid ja pildid on reas listina.
All lingil tuleb kõigi kasutajate pildid, mida muuta ei saa. Albumid ja pildid maatriksina.

Muutmisega - IndexEdit
*Näeb välja albumite nimekirjana
kui oled sisse loginud
	enda albumid
	kui oled admin või mode, siis kõik albumid

Vaatamine - Index
*Näeb välja nagu kena pildialbum
kui ei ole sisse loginud
sisse loginuna
	kõik teised albumid
		kui sa ei ole admin või mode

Üleval menüü "Minu albumid" tekib vaid sisse logides ja seal on alati kasutaja enda albumid (k.a. admini või mode enda albumid)
Esilehel on näha kõikide albumid ja sisse logides admin ja mode saavad neid muuta

26.03.2020
Kui registreerub uus kasutaja, luuakse talle automaatselt temanimeline tühi album, mis saab tema juurkaustaks. Kõikide kasutajate albumi vaates on siis kõigepealt kasutajate nimedega albumid ja seal sees nende enda tehtud albumid.
Albumi muutmine (Edit) on veel vigane. DateTime'ga mingi error
Albumi kustutamisel peaks küsima, kas kustutada ka alamalbumid.

04.04.2020
Pildi kustutamisel tema eemaldamine albumi kaanepiltide hulgast - tehtud
Esilehe kaanepildid linkidele (värskeimad, hinnatuimad, kasutajate albumid) - tehtud
Et viimast admini ei saaks kustutada ega talt staatust ära võtta - tehtud
Albumiteta kasutaja kustutamisel kustutatakse ja tema juurkaust - tehtud
Albumitega kasutaja kustutamise keeld - tehtud
Esilehe päise kujundus (taustapilt) - tehtud

Kasutaja kustutamine (nimekirjast admini poolt ja kusagilt ka kasutaja enda poolt) - keelatud
Albumi kustutamine (koos alamalbumite piltide, likede, kommentaaride ja märksõnadega) - keelatud



Navigeerimine pildi vaatest (eelmine, järgmine, üles)
Kommentaarid - Kalev?
Märksõnade lisamine
Albumi ja pildi otsimine nime, kirjelduse ja märksõna järgi

