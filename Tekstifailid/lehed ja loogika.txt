Main
külastajale
* alajaotus "värskeimad pildid". Kui klikkad pealkirjale (või lingile "vaata veel…"), avab galerii kõikide kasutajate piltidest kahanevas lisamise aja järjekorras
* alajaotus "populaarseimad pildid". Kui klikkad pealkirjale, avab galerii kõikide kasutajate piltidest kahanevas populaarsuse järjekorras
* äkki peaks tegema eraldi jaotused "enim hinnatud" ja "enim vaadatud"?
* kui kaks pilti on ühtviisi hinnatud (või samapalju vaadatud), siis jaotada lisamise järjekorras
* kasutajate albumid (viimati uuendatud või populaarseimad eespool)
kasutajale
* tema albumid ajalises või nime järjekorras - kasutaja ise saab valida


Album
külastajale
* pildid varem määratud suurusega maatriksis (ridu * veerge)
* kui albumis on rohkem kui maatriksisse mahub, siis jaotab mitmele lehele
kasutajale
* albumitele on võimalik lõpmatult alamalbumeid luua (andmebaasis väli parent_id - mis albumi alla ta käib. Kui on null, siis pealehel)
* albumi lisamine - millisesse albumisse, albumi nimi ja kirjeldus. Piltide sorteerimise alus ja suund
* albumit saab liigutada teise albumi alla
* piltide juurde tekib nupp "kustuta" (kas kõikide piltide juurde eraldi või checkbox'iga, nii et saab mitu korraga kustutada)
* saab valida mitu pilti korraga (checkbox) ja liigutada teise albumisse (combobox kasutaja albumitest koos nupuga Liiguta)


Picture
külastajale
* näitab pilti suuruses Large
* saab kommenteerida. Kui on külastaja, siis peab panema oma nime.
* saab hinnata (pärast hindamist sellelt IP-aadresilt kaob hindamise võimalus 1 tunniks. Hindamine lihtsalt Like, (mitte 1...5).
kasutajale
* Pildi lisamine:
* vali album, kuhu pilt lisada s.t. oled albumi vaates kasutajana sisse loginud, tekkib alla "Lisa uus pilt"
* pilt laaditakse andmebaasi
teha ja salvestada eraldi pildid orig_filename (algne ümbernimetatud pilt), large_filename (kuvamiseks sobivsuurus) ja thumb_filename (galerii vaates pisipilt)
* pildile saab valida pealkirja - kas algne failinimi (vaikimisi) või pandud nimi (caption)
* Pildi muutmine:
* muuda nime ja/või kirjeldust (labelid muutuvad textboxideks)
* nupp "kustuta"
* liiguta teise albumisse (combobox kasutaja albumitest koos nupuga Liiguta)

Header
* registreerimine ja sisselogimine
* breadcrumb albumites navigeerimiseks (pealeht > album1 > album2 > album3 > …). Albumitele sab klikata.

