﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestKonsool
{
    class Inimene
    {
        public string Nimi { get; set; }
        public override string ToString() => $"Tere {Nimi}!";
    }
}
