﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Galerii.Models;

namespace Galerii.Controllers
{
    public class HomeController : Controller
    {
        public static AspNetUser GetByEmail(string email)
        {
            using (GaleriiEntities db = new GaleriiEntities())
            {
                return db.AspNetUsers.Where(x => x.Email == email).SingleOrDefault();
            }
        }
        
        public ActionResult Index()
        {
            using (GaleriiEntities db = new GaleriiEntities())
            {
                if(Request.IsAuthenticated)
                {
                    var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                    ViewBag.userRoot = aspUser.UserRoot;
                }
                ViewBag.newestPic = db.DataFiles.OrderByDescending(x=>x.Id).Select(x=>x.Id).FirstOrDefault();
                var mostLikedPic = db.Likes.GroupBy(x => x.PictureId).OrderByDescending(x => x.Count()).Take(1).Select(x => x.Key).SingleOrDefault();
                ViewBag.mostLikedPic = db.Pictures.Where(x => x.Id == mostLikedPic).Select(x => x.DataFileId).SingleOrDefault();
                Random r = new Random();
                List<int> datafiles = new List<int>();
                foreach (int datafileId in db.DataFiles.Select(x=>x.Id))
                {
                    datafiles.Add(datafileId);
                }
                ViewBag.randomPic = datafiles[r.Next(datafiles.Count())];
                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Tere tulemast kasutama pildigaleriid!";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Kontaktandmed.";

            return View();
        }
    }
}