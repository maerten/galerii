﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Galerii.Models;

namespace Galerii.Controllers
{
    [Authorize(Roles = "Administraator")]
    public class UsersController : Controller
    {
        private GaleriiEntities db = new GaleriiEntities();

        // GET: Users       
        public ActionResult Index()
        {
            var aspNetUsers = db.AspNetUsers.Include("AspNetRoles");
            return View(aspNetUsers.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.Roles = db.AspNetRoles.ToList();
            return View(aspNetUser);
        }

        //add role
        public ActionResult AddRole(string id, string roleid)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleid);
            if(u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Add(r);
                    db.SaveChanges();
                }
                catch (Exception)
                {

                }
            }
            return RedirectToAction("Index", new { id });
        }

        //remove role
        public ActionResult RemoveRole(string id, string roleid)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleid);
            if (roleid !="1" || db.AspNetUsers.Where(x => x.AspNetRoles.Select(y => y.Id).Contains("1")).Count() > 1)
            {
                if (u != null && r != null)
                {
                    try
                    {
                        u.AspNetRoles.Remove(r);
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            return RedirectToAction("Index", new { id });
        }

        // GET: Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            if (db.Albums.Where(x => x.Owner == id).Where(x=>x.CategoryId != 3).Count() > 0) ViewBag.hasAlbums=true;
            else ViewBag.hasAlbums = false;
            if (aspNetUser.AspNetRoles.Select(y => y.Id).Contains("1") && db.AspNetUsers.Where(x => x.AspNetRoles.Select(y => y.Id).Contains("1")).Count() == 1) ViewBag.lastAdmin=true;
            else ViewBag.lastAdmin = false;
            return View(aspNetUser);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            Album userRoot = db.Albums.Where(x => x.Owner == id).Where(x => x.CategoryId == 3).SingleOrDefault();
            db.Albums.Remove(userRoot);
            db.AspNetUsers.Remove(aspNetUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
