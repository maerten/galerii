﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Galerii.Models;
using System.IO;

namespace Galerii.Controllers
{
    public class PicturesController : MyController
    {

        // GET: Pictures
        public ActionResult Index()
        {
            var pictures = db.Pictures.Include(p => p.Album).Include(p => p.AspNetUser).Include(p => p.Category).Include(p => p.DataFile).Include("Likes");
            //return View(pictures.AsEnumerable().Reverse().ToList());
            return View(pictures.OrderByDescending(x=>x.Created).ToList());
        }
        public ActionResult Gallery(int? likes)
        {
            var pictures = db.Pictures.Include(p => p.Album).Include(p => p.AspNetUser).Include(p => p.Category).Include(p => p.DataFile).Include("Likes");
            if (likes == 1)
            {
                ViewBag.header = "Hinnatuimad pildid";
                return View(pictures.Where(x => x.Likes.Count > 0).OrderByDescending(x => x.Likes.Count).ThenByDescending(x=>x.Created).ToList());
            }
            else
            {
                ViewBag.header = "Värskeimad pildid";
                return View(pictures.AsEnumerable().Reverse().ToList());
            }
        }

        // GET: Pictures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        [Authorize]
        public ActionResult AddLike(int? id)
        {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Pictures");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            try
            {
                //like.Add(new Like { LikerN = CurrentUser });
                Like like = db.Likes.Create();
                if (db.Likes.Any(l => l.Liker == CurrentUser.Id && l.PictureId == picture.Id || picture.Owner == CurrentUser.Id )) return View("Details", picture);
                {
                    like.Liker = CurrentUser.Id;
                    like.PictureId = picture.Id;
                    like.Created = DateTime.Now;
                    ///like.Number = 0;
                };
                db.Likes.Add(like);
                db.SaveChanges();
            }
            catch { }

            return View("Details", picture);
        }

        // POST: Add Comment
        [Authorize]
        public ActionResult AddComment(int id, string comment)
        {
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Pictures");
            db.Pictures.Find(id).Comments.Add(new Comment 
            { 
                Content = comment, 
                Commenter = CurrentUser.Id,
                Created =DateTime.Now
            });
            db.SaveChanges();
            return RedirectToAction("Details", new { id });
        }

        // GET: Pictures/Create
        [Authorize]
        public ActionResult Create(int parentId, string owner)
        {
            ViewBag.CategoryId = new SelectList(db.Categories.Where(x=>x.Id !=3), "Id", "Name", db.Albums.Where(x => x.Id == parentId).Select(x => x.CategoryId).SingleOrDefault());
            ViewBag.parentId = parentId;
            ViewBag.owner = owner;
            return View();
        }

        // POST: Pictures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int parentId, [Bind(Include = "Id,Name,Description,DataFileId,Owner,AlbumId,Created,CategoryId,State")] Picture picture, HttpPostedFileBase file)
        {
            var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var albumOwner = db.Albums.Where(x => x.Id == parentId).Select(x => x.AspNetUser).SingleOrDefault();
            if (albumOwner.Id == aspUser.Id || User.IsInRole("Administraator") || User.IsInRole("Moderaator"))
            {
                if (file != null)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        DataFile df = new DataFile
                        {
                            Content = br.ReadBytes(file.ContentLength),
                            ContentType = file.ContentType,
                            FileName = Path.GetFileName(file.FileName),
                            Created = DateTime.Now
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();

                        if (ModelState.IsValid)
                        {
                            picture.DataFileId = df.Id;
                            picture.Owner = db.Albums.Where(x => x.Id == parentId).Select(x => x.Owner).SingleOrDefault();
                            picture.AlbumId = parentId;
                            picture.Created = (DateTime)df.Created;
                            picture.State = 0;
                        }
                        db.Pictures.Add(picture);
                        db.SaveChanges();
                    }
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", picture.CategoryId);
            return RedirectToAction("IndexEdit", "Albums", new { parentId });
        }

        // GET: Pictures/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var pictureOwner = db.Pictures.Where(x => x.Id == id).Select(x => x.AspNetUser).SingleOrDefault();
            if (pictureOwner.Id == aspUser.Id || User.IsInRole("Administraator") || User.IsInRole("Moderaator"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Picture picture = db.Pictures.Find(id);
                if (picture == null)
                {
                    return HttpNotFound();
                }
                ViewBag.AlbumId = new SelectList(db.Albums.Where(x => x.Owner == picture.Owner), "Id", "Name", picture.AlbumId);
                ViewBag.CategoryId = new SelectList(db.Categories.Where(x => x.Id != 3), "Id", "Name", picture.CategoryId);
                ViewBag.CoverForAlbumId = new SelectList(db.Albums.Where(x => x.Owner == picture.Owner), "Id", "Name");
                return View(picture);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: Pictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,DataFileId,Owner,AlbumId,Created,CategoryId,State")] Picture picture)
        {
            if (ModelState.IsValid)
            {
                db.Entry(picture).State = EntityState.Modified;
                db.Entry(picture).Property("DataFileId").IsModified = false;
                db.Entry(picture).Property("Owner").IsModified = false;
                db.Entry(picture).Property("Created").IsModified = false;
                db.Entry(picture).Property("State").IsModified = false;
                db.SaveChanges();
            }            
            if (Request.Form["CoverForAlbumId"] != "")
            {
                int albumId = int.Parse(Request.Form["CoverForAlbumId"]);
                var album = db.Albums.Find(albumId);
                album.CoverPictureId = picture.Id;
                db.SaveChanges();
            }
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name");
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name");
            int parentId = db.Albums.Where(x => x.Id == picture.AlbumId).Select(x => x.Id).SingleOrDefault();
            return RedirectToAction("IndexEdit", "Albums", new { parentId });
        }

        // GET: Pictures/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var pictureOwner = db.Pictures.Where(x => x.Id == id).Select(x => x.AspNetUser).SingleOrDefault();
            if (pictureOwner.Id == aspUser.Id || User.IsInRole("Administraator") || User.IsInRole("Moderaator"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Picture picture = db.Pictures.Find(id);
                if (picture == null)
                {
                    return HttpNotFound();
                }
                return View(picture);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: Pictures/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Picture picture = db.Pictures.Find(id);           
            DataFile df = db.DataFiles.Find(picture.DataFileId);

            var hasCoverPic = db.Albums.Where(x => x.CoverPictureId == id).ToList();
            foreach (var album in hasCoverPic)
            {
                album.CoverPictureId=null;
            }

            List<Like> likes = db.Likes.Where(x => x.PictureId == id).ToList();
            foreach (var like in likes)
            {
                db.Likes.Remove(like);
            }

            //List<Comment> comments = db.Comments.Where(x => x.PictureId == id).ToList();
            //foreach (var comment in comments)
            //{
            //    db.Comments.Remove(comment);
            //}  
            
            db.Pictures.Remove(picture);
            db.DataFiles.Remove(df);
            db.SaveChanges();
            int parentId = db.Albums.Where(x => x.Id == picture.AlbumId).Select(x => x.Id).SingleOrDefault();
            return RedirectToAction("IndexEdit", "Albums", new { parentId });
        }
        public ActionResult Content(int? id)
        {
            DataFile f = db.DataFiles.Find(id ?? 0);
            if (f == null) return HttpNotFound();
            return File(f.Content, f.ContentType);
        }
    }
}
