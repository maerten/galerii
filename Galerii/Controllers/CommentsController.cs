﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Galerii.Models;

namespace Galerii.Controllers
{
    public class CommentsController : Controller
    {
        private GaleriiEntities db = new GaleriiEntities();

        // GET: Comments
        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.Album).Include(c => c.AspNetUser).Include(c => c.Comment2).Include(c => c.Picture);
            return View(comments.ToList());
        }

        // GET: Comments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // GET: Comments/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name");
            ViewBag.Commenter = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.CommentId = new SelectList(db.Comments, "Id", "Commenter");
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "Name");
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id,Commenter,Content,Created,PictureId,AlbumId,CommentId")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name", comment.AlbumId);
            ViewBag.Commenter = new SelectList(db.AspNetUsers, "Id", "Email", comment.Commenter);
            ViewBag.CommentId = new SelectList(db.Comments, "Id", "Commenter", comment.CommentId);
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "Name", comment.PictureId);
            return View(comment);
        }

        // GET: Comments/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name", comment.AlbumId);
            ViewBag.Commenter = new SelectList(db.AspNetUsers, "Id", "Email", comment.Commenter);
            ViewBag.CommentId = new SelectList(db.Comments, "Id", "Commenter", comment.CommentId);
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "Name", comment.PictureId);
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id,Commenter,Content,Created,PictureId,AlbumId,CommentId")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name", comment.AlbumId);
            ViewBag.Commenter = new SelectList(db.AspNetUsers, "Id", "Email", comment.Commenter);
            ViewBag.CommentId = new SelectList(db.Comments, "Id", "Commenter", comment.CommentId);
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "Name", comment.PictureId);
            return View(comment);
        }

        // GET: Comments/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // POST: Comments/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
