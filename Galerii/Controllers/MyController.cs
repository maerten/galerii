﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Galerii.Models;

namespace Galerii.Controllers
{
    public class MyController : Controller
    {

        protected GaleriiEntities db = new GaleriiEntities();
        AspNetUser currentUser = null;
        protected AspNetUser CurrentUser =>
            currentUser ??
            (currentUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault());


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
