﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Galerii.Models;
using System.IO;

namespace Galerii.Controllers
{
    public class AlbumsController : Controller
    {
        private GaleriiEntities db = new GaleriiEntities();

        public int parentId=0;
        
        //AspNetUser currentUser = null;
        //AspNetUser CurrentUser =>
        //    currentUser ??
        //    (currentUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault());

        // GET: Albums
        public ActionResult Index(int? parentId)
        {
            ViewBag.parentId = parentId;
            ViewBag.parentName = db.Albums.Where(x => x.Id == parentId).Select(x => x.Name).SingleOrDefault();
            var albums = db.Albums.Include(a => a.Category).Include(a => a.Picture).Include(a => a.AspNetUser).Include(a => a.Album2);
            var pictures = db.Pictures.Include(p => p.Album).Include(p => p.AspNetUser).Include(p => p.Category).Include(p => p.DataFile).Where(x => x.AlbumId == parentId);
            ViewBag.pictures = pictures;
            ViewBag.picsInAlbum = pictures.Count();
            return View(albums.Where(p => p.Album2.Id == parentId).ToList());                       
        }

        [Authorize]
        public ActionResult IndexEdit(int? parentId)
        {           
            var albums = db.Albums.Include(a => a.Category).Include(a => a.Picture).Include(a => a.AspNetUser).Include(a => a.Album2);
            var pictures = db.Pictures.Include(p => p.Album).Include(p => p.AspNetUser).Include(p => p.Category).Include(p => p.DataFile).Where(x=>x.AlbumId==parentId);
            var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var owner = db.Albums.Where(x => x.Id == parentId).Select(x => x.AspNetUser.Email).SingleOrDefault();
            ViewBag.pictures = pictures;
            ViewBag.picsInAlbum = pictures.Count();
            ViewBag.albumsInAlbum = albums.Where(x=>x.ParentAlbumId==parentId).Count();
            ViewBag.aspUser = aspUser.Email;
            ViewBag.parentId = parentId;
            ViewBag.owner = owner;
            Album upLevel = db.Albums.Where(x => x.Id == parentId).SingleOrDefault();
            if(upLevel != null)
            {
                ViewBag.upLevelId = upLevel.ParentAlbumId;
                ViewBag.upLevelName = db.Albums.Where(x => x.Id == upLevel.ParentAlbumId).Select(x => x.Name).SingleOrDefault();
            }            
            ViewBag.parentName = db.Albums.Where(x => x.Id == parentId).Select(x => x.Name).SingleOrDefault();
            ViewBag.catId = db.Albums.Where(x => x.Id == parentId).Select(x => x.CategoryId).SingleOrDefault();
            if(aspUser.Email==owner || User.IsInRole("Administraator") || User.IsInRole("Moderaator"))
            {
                return View(albums.Where(p => p.AspNetUser.Email == owner).Where(p => p.Album2.Id == parentId).ToList());
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Albums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // GET: Albums/Create
        [Authorize]
        public ActionResult Create(int parentId)
        {
            ViewBag.CategoryId = new SelectList(db.Categories.Where(x => x.Id != 3), "Id", "Name", db.Albums.Where(x => x.Id == parentId).Select(x => x.CategoryId).SingleOrDefault());
            //ViewBag.CoverPictureId = new SelectList(db.Pictures, "Id", "Name");
            return View();
        }
        // POST: Albums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int parentId, [Bind(Include = "Id,Name,Created,Owner,CoverPictureId,State,CategoryId,ParentAlbumId")] Album album)
        {
            var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var albumOwner = db.Albums.Where(x => x.Id == parentId).Select(x => x.AspNetUser).SingleOrDefault();
            if (albumOwner.Id==aspUser.Id || User.IsInRole("Administraator") || User.IsInRole("Moderaator"))
            {
                if (ModelState.IsValid)
                {
                    album.Owner = albumOwner.Id;
                    album.Created = DateTime.Now;
                    album.State = 0;
                    album.ParentAlbumId = parentId;
                    db.Albums.Add(album);
                    db.SaveChanges();
                    return RedirectToAction("IndexEdit", new { parentId});
                } 
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", album.CategoryId);
            ViewBag.ParentAlbumId = new SelectList(db.Albums, "Id", "Name", album.ParentAlbumId);
            return View(album);
        }

        // GET: Albums/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var albumOwner = db.Albums.Where(x => x.Id == id).Select(x => x.AspNetUser).SingleOrDefault();
            if (albumOwner.Id == aspUser.Id || User.IsInRole("Administraator") || User.IsInRole("Moderaator"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Album album = db.Albums.Find(id);
                if (album == null)
                {
                    return HttpNotFound();
                }
                ViewBag.CategoryId = new SelectList(db.Categories.Where(x => x.Name != "UserRoot"), "Id", "Name", album.CategoryId);
                ViewBag.ParentAlbumId = new SelectList(db.Albums.Where(x => x.Owner == album.Owner), "Id", "Name", album.ParentAlbumId);
                return View(album);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: Albums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Created,Owner,CoverPictureId,State,CategoryId,ParentAlbumId")] Album album)
        {
            var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            string owner = aspUser.Email;
            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.Entry(album).Property("Owner").IsModified = false;
                db.Entry(album).Property("Created").IsModified = false;                
                db.SaveChanges();
                return RedirectToAction("IndexEdit","Albums", new { parentId = album.ParentAlbumId});
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", album.CategoryId);
            ViewBag.ParentAlbumId = new SelectList(db.Albums, "Id", "Name", album.ParentAlbumId);
            return View(album);
        }

        // GET: Albums/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (db.Albums.Where(x => x.ParentAlbumId == id).Count() == 0 && db.Pictures.Where(x => x.AlbumId == id).Count() == 0) ViewBag.childless = true;
            else ViewBag.childless = false;
            var aspUser = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var albumOwner = db.Albums.Where(x => x.Id == id).Select(x => x.AspNetUser).SingleOrDefault();
            if (albumOwner.Id == aspUser.Id || User.IsInRole("Administraator") || User.IsInRole("Moderaator"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Album album = db.Albums.Find(id);
                if (album == null)
                {
                    return HttpNotFound();
                }
                return View(album);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: Albums/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Album album = db.Albums.Find(id);
            int parentId = (int)album.ParentAlbumId;
            db.Albums.Remove(album);
            db.SaveChanges();
            return RedirectToAction("IndexEdit", "Albums", new { parentId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
