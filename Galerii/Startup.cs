﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Galerii.Startup))]
namespace Galerii
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
